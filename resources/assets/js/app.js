
require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router';

window.Vue.use(VueRouter);

import crud from './components/CRUD.vue';


const routes = [
	{
            path: '/',
            components: {
            	view_crud:crud
            }
    }
]

const router = new VueRouter({ routes })

const app = new Vue({
    router
}).$mount('#app');
