<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\tbl_vehiculo;
use App\Models\tbl_profesional;
use Carbon\Carbon;

class BetterController extends Controller
{
    public function PostProfesional(Request $request){

    	$profesional = tbl_profesional::create([
    		'nombre_apellidos'=> $request['nombre_apellidos'],
    		'cedula'=> $request['cedula'], 
    		'fecha_nacimiento'=> $request['fecha_nacimiento'],
    		'profesion'=> $request['profesion'],
    		'direccion'=> $request['direccion'], 
    		'municipio'=> $request['municipio'], 
    		'telefono'=> $request['telefono'], 
    		'sexo'=> $request['sexo'],
    	]);

    	tbl_vehiculo::create([    		
    		'id_profesional'=> $profesional->idProfesional,
    		'categoria'=> $request['categoria'],
    		'marca'=> $request['marca'],
    		'año'=> $request['año'],
    	]);

    	

    	return [ "status" => "success", "message" => "Datos Registrados"];
    }


    public function GetDataProfesional(){
    	return \DB::table('tbl_profesional')->join('tbl_vehiculo', 'tbl_profesional.idProfesional','=','tbl_vehiculo.id_profesional')->select('tbl_vehiculo.*','tbl_profesional.*')->get();
    }

    public function BusquedaProfesional(Request $request){
    	$profesional= $request['buscarprofesional'];

        if(isset($profesional)) {

        	return \DB::table('tbl_profesional')->join('tbl_vehiculo', 'tbl_profesional.idProfesional','=','tbl_vehiculo.id_profesional')->where('tbl_profesional.nombre_apellidos', 'like', '%'. $profesional . '%')->select('tbl_vehiculo.*','tbl_profesional.*')->get();

        }else{
            
            return \DB::table('tbl_profesional')->join('tbl_vehiculo', 'tbl_profesional.idProfesional','=','tbl_vehiculo.id_profesional')->select('tbl_vehiculo.*','tbl_profesional.*')->get();
        }
    }



    public function UpdateProfesional(Request $request){
    	$id_profesional=$request['id_profesional'];

        $profesional=tbl_profesional::where('idProfesional', $id_profesional)->update([
        	'nombre_apellidos'=> $request['nombre_apellidos'],
    		'cedula'=> $request['cedula'], 
    		'fecha_nacimiento'=> $request['fecha_nacimiento'],
    		'profesion'=> $request['profesion'],
    		'direccion'=> $request['direccion'], 
    		'municipio'=> $request['municipio'], 
    		'telefono'=> $request['telefono'], 
    		'sexo'=> $request['sexo'],
        ]);

        $vehiculo=tbl_vehiculo::where('id_profesional', $id_profesional)->update([
    		'categoria'=> $request['categoria'],
    		'marca'=> $request['marca'],
    		'año'=> $request['año'],
        ]);


        if ($profesional==true && $vehiculo==true) {
            return [ "status" => "success", "message" => "Datos Actualizado"];
        }else{
            return [ "status" => "error", "message" => "Error al Actualizar"];
        }
    }


    public function DeleteProfesional($id_profesional){


    	$vehiculo=tbl_vehiculo::where('id_profesional', $id_profesional)->delete();
    	$profesional = tbl_profesional::where('idProfesional', $id_profesional)->delete();
    	

        if ($profesional==true && $vehiculo==true) {
            return [ "status" => "success", "message" => "Elimnacion Exitoso"];
        }else{
            return [ "status" => "error", "message" => "Error al Eliminar"];
        }
    }
}
